import jwtdecode from 'jwt-decode';
import qs from 'qs';
import { pick } from 'lodash-es';

export function authFromUrl() {
    let { search: query, hash, href } = self.location;

    const url = new URL(href);

    if (typeof query === "string") query = qs.parse(query.substr(1));
    if (typeof hash === "string") hash = qs.parse(hash.substr(1));

    const source = Object.assign({}, query, hash);
    const token = source.access_token || source.token;

    if (!token) return { strippedUrl: url.toString() };

    const extra = ["lifetime", "expires", "scope", "user"];
    const strip = [
        source.access_token ? "access_token" : "token",
        ...extra,
    ];
    const data = Object.assign({
        token,
        type: "bearer",
    }, pick(source, extra));
    strip.forEach(function (key) {
        delete query[key];
        delete hash[key];
    });

    url.search = qs.stringify(query);
    url.hash = qs.stringify(hash);

    if (data) {
        var jwt = null;
        try {
            jwt = jwtdecode(data.token);

            if (jwt.exp) {
                data.expires = new Date(jwt.exp * 1000).toISOString();
                delete jwt.exp;
            }

            if (jwt.sub) {
                data.user = jwt.sub;
                delete jwt.exp;
            }

            Object.assign(data, jwt);
        } catch (e) {
        }
    }

    return { data, strippedUrl: url.toString() };
}
