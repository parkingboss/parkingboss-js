import storage from 'store';
import jwt_decode from 'jwt-decode';

const AUTH_KEY = "user/auth";

export function getAuth(skipExpiryCheck) {
    var data = storage.get(AUTH_KEY);

    if (!(data && data.token)) {
        // No Session
        return null;
    }

    let jwt = null;
    try {
        jwt = jwt_decode(data.token);
    } catch (e) {
        console.warn("Failed to Decode JWT; forcing login", e);
    }

    if (jwt == null || (!skipExpiryCheck && jwt.exp && jwt.exp * 1000 < Date.now())) {
        // Bad Token, or Token Expired
        return null;
    }

    return data;
}

export function getAuthHeader(skipExpiryCheck) {
    const auth = getAuth(skipExpiryCheck);

    if (!auth) return '';

    return [auth.type, auth.token].filter(x => x).join(' ');
}

export function setAuth(data) {
    storage.set(AUTH_KEY, data);
}

export function removeAuth() {
    storage.remove(AUTH_KEY);
}
