import { get, matches, pickBy } from 'lodash-es';
import { isWithinRange, isBefore, isAfter } from 'date-fns';

export function fetchAuthorizations(api, query = {}) {
    return api.fetch('GET', `/authorizations`, null, true, query);
}

export function normalizeAuthorizationsPayload(a) {
    return {
        sysAdmin: !!a.authorizations.system,
        authorizations: Object.values(a.authorizations.items)
            .map(x => a.items[x] || x)
            .map(x => Object.assign({}, x, {
                user: a.items[x.principal] || a.users.items[x.principal],
            })),
    };
}

export async function loadAuthorizations(api, query = {}) {
    const authPayload = await fetchAuthorizations(api, query);
    return normalizeAuthorizationsPayload(authPayload);
}

export function authorizationsLoader(api) {
    return (query = {}) => loadAuthorizations(api, query);
}

export function roles(auths, { principal, scope, at }) {
    if (auths.sysAdmin) {
        return { admin: true, owner: true, patrol: true };
    }

    let matchingAuths = auths.authorizations
        .filter(matches(pickBy({ principal, scope })))
        .filter(a => {
            if (at == null) return true;
            at = new Date(at);

            const min = get(a, 'valid.min.datetime');
            const max = get(a, 'valid.max.datetime');

            if (min == null && max == null) return true;
            if (min == null) return isBefore(at, new Date(max));
            if (max == null) return isAfter(at, new Date(min));
            return isWithinRange(at, new Date(min), new Date(max));
        });
    if (matchingAuths.length == 0) {
        return { admin: false, owner: false, patrol: false };
    }
    if (matchingAuths.length == 1) {
        return Object.assign({}, matchingAuths[0].roles);
    }
    console.group('Multiple Auth Matches!');
    console.error(matchingAuths, { principal, scope, at });
    console.groupEnd();
    throw new Error('Multiple authorizations match your roles query.');
}