const BASE_QUERY = "link[rel~='index'][rel~='api'][type~='application/json']";

let _base = null;
let _baseLink = null;

function getBaseLink() {
    _baseLink = document.querySelector(BASE_QUERY);
    if (!_baseLink) {
        throw new Error(`Base not set, and no <${BASE_QUERY}> found`);
    }
    return _baseLink;
}

function setFromBaseLink() {
    _base = _baseLink && _baseLink.href;
}

function setBase(newBase) {
    _base = newBase;
}

function getBase() {
    if (_base == null) {
        _baseLink = getBaseLink();
        const observer = new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                if (mutation.attributeName === 'href') {
                    setFromBaseLink();
                }
            });
        });
        observer.observe(_baseLink, { attributes: true });
        setFromBaseLink();
    }
    return _base;
}

export function base(val) {
    if (val != null) {
        setBase(val);
    }
    return getBase();
}
