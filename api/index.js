import { authFromUrl } from './auth-from-url';
import { base } from './base';
import { getAuth, setAuth, removeAuth, getAuthHeader } from './auth-storage';
import { pick, pickBy, invoke } from 'lodash-es';
import jwt_decode from 'jwt-decode';
import qs from 'qs';

function isAuthData(data) {
    if (!(data && data.token && data.type)) {
        return false;
    }
    try {
        jwt_decode(data.token);
        return true;
    } catch (e) {
        console.warn("token was not a valid jwt", e);
        return false;
    }
}

function loginUrl() {
    const currUrl = encodeURI(window.location.toString());
    return `https://my.parkingboss.com/user/navigate?url=${currUrl}`
}

async function resultJson(response) {
    let result;
    try {
        result = await response.clone().json();
    } catch (e) {
        result = { body: await response.text() };
    }

    let date = response.headers.get("Date") || response.headers.get("date");
    if (date) {
        date = new Date(date).toISOString();
    }

    let modified = response.headers.get("Last-Modified") || response.headers.get("last-modified");
    if (modified) {
        modified = new Date(modified).toISOString();
    }
    let status = response.status;

    return Object.assign(result, {
        date,
        modified,
        status,
    });
}

export default function Api({ noRewrite, noRedirect, noIntercom, apiBase } = {}) {
    let { data, strippedUrl } = authFromUrl();

    if (!isAuthData(data)) {
        data = getAuth() || {};
    } else {
        setAuth(data);
    }

    if (!noRedirect && !isAuthData(data)) {
        window.location = loginUrl();
    }
    if (!noRewrite && self.history && self.history.replaceState) {
        self.history.replaceState(null, null, strippedUrl);
    }
    if (!noIntercom) {
        invoke(window, 'Intercom.update', pick(data, 'name', 'email'));
    }
    if (apiBase) {
        base(apiBase);
    }

    return {
        base(newVal) {
            return base(newVal);
        },
        logOut() {
            removeAuth();
        },
        authData() {
            return data;
        },
        async fetch(method, url, body, header, query = {}) {
            var requested = new Date().toISOString();

            const args = [
                method,
                url,
                body,
                header === true ? getAuthHeader() : header,
                query
            ];
            [method, url, body, header, query] = await Promise.all(args);
            query.Authorization = header;

            const fullUrl = new URL(base() + url);
            fullUrl.search = qs.stringify(query);

            let result;
            try {
                result = await self.fetch(fullUrl.toString(), pickBy({ method, body }));
            } catch (e) {
                throw Object.assign(new Error('Network Error'), { url: fullUrl.toString() });
            }
            const json = await resultJson(result);

            if (json) json.requested = requested;

            if (result.ok) {
                return json;
            } else {
                throw Object.assign(new Error("API Error"), json);
            }
        }
    };
}