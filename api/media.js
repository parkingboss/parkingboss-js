export function fetchMedia(api, id, query = {}) {
    return api.fetch('GET', `/locations/${id}/media`, null, true, query);
}

export function normalizeMediaPayload(m) {
    return m.media;
}

export async function loadMedia(api, id, query = {}) {
    const mediaPayload = await fetchMedia(api, id, query);
    return normalizeMediaPayload(mediaPayload);
}

export function mediaLoader(api) {
    return (id, query = {}) => loadMedia(api, id, query);
}