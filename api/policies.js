export function fetchPolicies(api, id, query = {}) {
    return api.fetch('GET', `/locations/${id}/permits/issuers`, null, true, query);
}

export async function loadPolicies(api, id, query = {}) {
    return await fetchPolicies(api, id, query);
}

export function policiesLoader(api) {
    return (id, query = {}) => loadPolicies(api, id, query);
}