export function fetchProperties(api, query = {}) {
    return api.fetch('GET', `/locations/${id}`, null, true, query);
}

export function fetchProperty(api, id, query = {}) {
    return api.fetch('GET', `/locations/${id}`, null, true, query);
}

export function getProperty(payload, id = null) {
    return payload.items[id || payload.locations.item] || payload.locations.items[id || payload.locations.item];
}

export function getAddress(payload, id = null) {
    return payload.items[id || payload.addresses.item] || payload.addresses.items[id || payload.addresses.item];
}

export function normalizeProperty(payload) {
    const property = getProperty(payload);
    const address = getAddress(payload);
    return Object.assign({}, property, { address });
}

export function normalizeProperties(payload) {
    return Object.values(payload.locations.items)
        .map(prop => payload.items[prop] || prop)
        .map(prop => Object.assign({}, prop, {
            address: getAddress(payload, prop.address)
        }));
}

export async function loadProperty(api, id, query = {}) {
    const payload = await fetchProperty(api, id, query);
    return normalizeProperty(payload);
}

export async function loadProperties(api, query = {}) {
    const payload = await fetchProperties(api, query);
    return normalizeProperties(payload);
}

export function propertyLoader(api) {
    return (id, query = {}) => loadProperty(api, id, query);
}

export function propertiesLoader(api) {
    return (query = {}) => loadProperties(api, query);
}