const NONDIGIT = /\D/g;
const FIRST3 = /^(\d{3})/;
const LAST4 = /(\d{4})$/;

export function formatPhone(num) {
    if (!num) return num;
    const justNums = num.replace(NONDIGIT, '');
    if (justNums.length === 7) {
        return justNums
            .replace(FIRST3, '$1-');
    }
    if (justNums.length === 10) {
        return justNums
            .replace(FIRST3, '$1-')
            .replace(LAST4, '-$1');
    }
    return num;
}