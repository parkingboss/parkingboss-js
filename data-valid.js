import { format as formatDate } from 'date-fns';
import { parse as parseDuration, toSeconds } from 'iso8601-duration';
import { each, invoke } from 'lodash-es';

export function dataValid() {
    function expired(time) {

        if(!time) return;

        const datetime = time.getAttribute("datetime");

        const invalidate = time.getAttribute("data-valid");

        const ms = 0;
        if(!!invalidate) {
            const duration = parseDuration(invalidate);
            const seconds = toSeconds(duration);
            ms = seconds * 1000;
        }

        if(!datetime || ms <= 0) {
            time.classList.remove("invalid");
            return;
        }

        // if now ms is after datetime ms + period ms
        time.classList[Date.now() > new Date(datetime).getTime() + ms ? "add" : "remove"]("invalid");
    }

    setInterval(function() {
        Array.from(document.querySelectorAll("time[data-valid]")).forEach(expired);
    }, 1 * 60 * 1000);

    each([ "change", "update"], function(evt) {
        document.documentElement.addEventListener(evt, function(e) {
            if(!invoke(e, "target.matches", "time[data-valid]")) return;

            const time = e.target;

            const datetime = time.getAttribute("datetime");
            time.innerHTML = !!datetime ? formatDate(new Date(datetime), 'ddd MMM D h:mm A [(]Z[)]') : "";

            expired(time);
        });
    });
}
