﻿// Does _not_ have a hard dependency on Flatpickr. To use pass in an imported Flatpickr:
// ```
// import Flatpickr form 'flatpickr';
// import { setupFlatpickrInputs } from 'parking-boss-lib/datepicker-flatpickr'
// setupFlatpickrInputs(Flatpickr);
// ```
import { each } from 'lodash-es';

export function setupFlatpickrInputs(Flatpickr) {
       // don't use if touch
       var touch = ('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch);

       if(!!touch) return;

       each(document.documentElement.querySelectorAll("input[type='date']"), function(input) {

           var label = input.closest("label");
           var cal = document.createElement("aside");
           cal.classList.add("calendar");
           label.parentNode.append(cal);
           var container = document.createElement("input");
           container.setAttribute("type", "hidden");
           cal.append(container);
           var active = false;

           var flatpickr = new Flatpickr(container, {
               //static:true,
               inline:true,
               //minDate:"today",
               onChange: function(dateObj, dateStr, instance) {
                   input.value = dateStr;
                   input.dispatchEvent(new CustomEvent("change", { bubbles:true }));
               },
               prevArrow:'<button type="button" class="previous"></button>',
               nextArrow:'<button type="button" class="next"></button>',
           });

           input.setAttribute("readonly", "readonly");
           input.readonly = true;

           each([ "click", "focusin", "focus" ], function(on) {
               input.addEventListener(on, function(e) {
                   e.preventDefault();
                   if(cal.classList.contains("active")) return;
                   if(!!active) return;

                   //console.log(input.value);
                   var min = input.min || input.getAttribute("min") || null;
                   if(!!min) flatpickr.set("minDate", new Date(min));
                   var max = input.max || input.getAttribute("max") || null;
                   if(!!max) flatpickr.set("maxDate", new Date(max));

                   if(!!input.value) flatpickr.setDate(input.value);
                   cal.classList.add("active");
                   active = true;

                   input.blur();
               });
           });

           var deactivate = function() {
               active = false;
               cal.classList.remove("active");
           }

           // let's watch for mutation
           if(!!window.MutationObserver) {
               var observer = new MutationObserver(function(mutations) {
                   if(!!input.disabled) deactivate();
               });

               observer.observe(input, { attributes: true, attributeFilter: [ "disabled" ] });
           }

           input.addEventListener("change", function(e) {
               if(!this.value) return;

               deactivate();

               this.blur();
           });
       });
   }