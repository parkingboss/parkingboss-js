import {
    each,
    invoke,
    range,
    reduce,
} from 'lodash-es';
import {
    format,
    isSameDay,
    addDays,
    addYears,
    addMilliseconds,
    subMilliseconds,
} from 'date-fns';
import {
    parse as parseDuration
} from 'iso8601-duration';

export function dates() {
    // disabled is a reflected attribute-property
    function disable(elem) {
        elem.disabled = true;
        elem.closest("label").classList.add("disabled");
    }
    function enable(elem) {
        elem.disabled = false;
        elem.closest("label").classList.remove("disabled");
    }

    document.documentElement.addEventListener("change", function(e) {
        if(!invoke(e, "target.matches", "form .datetime select.date")) return;

        var target = e.target

        if(target.options.length <= 0) return; // no options

        var datetime = target.closest(".datetime");

        var inputs = datetime.querySelectorAll("input[type='date']");
        var times = datetime.querySelectorAll("select.time");

        each(inputs, disable);

        target.blur();

        enable(target);
        var label = target.closest("label");

        if(target.value == "0" && target.selectedIndex === 0 && !!target.querySelector("option[value='0']")) {

            // now
            label.classList.add("now");

            // disable time
            each(times, disable);
            each(inputs, disable);

        } else {

            // not now
            label.classList.remove("now");

            // activate time
            each(times, function(select) {
                enable(select);
                select.dispatchEvent(new CustomEvent("change", { bubbles:true }));
            });
            each(inputs, disable);
        }

        if(target.value != "") return;
        if(!target.options[target.options.length - 1].value && target.selectedIndex < target.options.length - 1) return; //if last item has no value and not selected, don't continue

        // disable target
        if(!!target.name) disable(target);

        var touch = (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch));

        // activate date inputs
        each(inputs, function(input) {
            enable(input);
            input.value = input.value || input.getAttribute("value"); // refresh from value;
            input.dispatchEvent(new CustomEvent("change", { bubbles: true }));
            if(!!touch) return;
            try { input.focus(); } catch (ex) { }
        });

    });


    function setDateTime(fieldset, value, setAsDefault, minimum) {
        setAsDefault = !(setAsDefault === false);

        var now = new Date();

        var date = format(new Date(value), "YYYY-MM-DD");

        var dateset = false;

        var dateSelect = fieldset.querySelector("select.date");
        if(!dateset && !!dateSelect) {

            var selectDate = date;

            if (isSameDay(value, now)) selectDate = "0";
            if (isSameDay(value, addDays(now, 1))) selectDate = "+1";

            each(dateSelect.options, function(option) {

                if(option.value == selectDate) {
                    if(!!setAsDefault) option.setAttribute("selected", "selected");
                    dateSelect.selectedIndex = option.index;
                    dateSelect.value = option.value;
                } else {
                    if(!!setAsDefault) option.removeAttribute("selected");
                }

            });
            dateSelect.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        }

        var dateInput = fieldset.querySelector("input[type='date']");
        if(!dateset && !!dateInput) {
            if(!!setAsDefault) dateInput.setAttribute("value", date);
            dateInput.value = date;
            dateInput.dispatchEvent(new CustomEvent("change", { bubbles: true }));

            if(!!minimum) {
                var min = format(minimum, "YYYY-MM-DD");
                var max = format(addYears(minimum, 10), "YYYY-MM-DD");
                dateInput.min = min;
                dateInput.max = max;
                if(!!setAsDefault) dateInput.setAttribute("min", min);
                if(!!setAsDefault) dateInput.setAttribute("max", max);
            }
        }

        var time = format(value, "HH:00");
        var timeSelect = fieldset.querySelector("select.time");
        if(!!timeSelect) {
            each(timeSelect.options, function(option) {

                if(option.value == time) {
                    if(!!setAsDefault) option.setAttribute("selected", "selected");
                    timeSelect.selectedIndex = option.index;
                    timeSelect.value = option.value;
                } else {
                    if(!!setAsDefault) option.removeAttribute("selected");
                }

            });
            timeSelect.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        }

    };

    // init date pickers - THIS EXECUTES IMMEDIATELY
    var dateInit = new Promise(function(resolve) {

        each(document.querySelectorAll("select.date:empty"), function(select) {
            select.innerHTML = '<option value="0" label="Right now" selected="selected">Right now</option><option value="0" label="Later today">Today</option><option value="+1">Tomorrow</option>' + reduce(range(2, 7), function(html, i) {
                var m = addDays(new Date(), i);

                return html + ('<option value="' + format(m, "YYYY-MM-DD") + '">' + format(m, "ddd, MMM D") + '</option>');
            }, "") + '<option value="">Later date&hellip;</option>';

            select.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        });


        each(document.querySelectorAll("select.time"), function(select) {
            select.innerHTML = reduce(range(0, 23 + 1), function(html, h) {
                var m = setMinutes(setHours(new Date(), h), 0);
                return html + '<option value="' + format(m, "HH:mm") + '">' + format(m, "h A") + '</option>';
            }, "");

            select.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        });


        var freeFormDateStart = format(addDays(new Date(), 7), "YYYY-MM-DD");

        each(document.querySelectorAll("input[type='date']"), function(input) {
            if(!!input.value) return;
            input.setAttribute("value", input.value = freeFormDateStart);
            input.dispatchEvent(new CustomEvent("change", { bubbles: true }));
        });

        each(document.querySelectorAll("fieldset.datetime"), function(dateTime) {
            var date = new Date();

            var future = dateTime.getAttribute("data-future")
            if(!!future) date = addMilliseconds(date, parseDuration(future));

            var past = dateTime.getAttribute("data-past");
            if(!!past) date = subMilliseconds(date, parseDuration(past));

            setDateTime(dateTime, date, true, !!future ? new Date() : null);
        });

        resolve();
    });

}