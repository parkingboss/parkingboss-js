import {
    each,
    invoke,
} from 'lodash-es';
import {
    isValid,
    format
} from 'date-fns';

export function form() {
    document.documentElement.addEventListener("change", function(e) {
        if(!invoke(e, "target.matches", "select")) return;

        var select = e.target;
        select.blur();

        var txt = select.parentNode.querySelector("select+span");
        if(!txt) select.after(txt = document.createElement("span"));

        var option = select.options[select.selectedIndex || 0];
        if(!!option) txt.textContent = option.title || option.textContent || option.text;
    });

    document.documentElement.addEventListener("change", function(e) {
        if(!invoke(e, "target.matches", "input[type='date']")) return;

        var target = e.target;

        var date = new Date(target.value);

        var txt = isValid(date) ? format(date, target.getAttribute("data-format") || "ddd, MMM D, YYYY") : "";
        var span = target.parentNode.querySelector("input[type='] date, + span");

        if (!span) {
            span = document.createElement("span");
            target.after(span);
        }

        span.textContent = txt; // only change if something new
    });

    var onchange = function(e) {
        var target = e.target;
        var label = target.closest("label");
        if(!!label) label.classList[!!target.value ? "add" : "remove"]("value");
    };

    each([ "input", "textinput", "change", "keypress", "keyup", "keydown"], function(ev) {
        document.documentElement.addEventListener(ev, function(e) {
            if(!invoke(e, "target.matches", "input,textarea,select")) return;

            onchange(e);
        });
    });

    document.documentElement.addEventListener("focus", function(e) {
        if(!invoke(e, "target.matches", "input,textarea,select")) return;

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.add("focused");

        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.add("focused");
    });

    document.documentElement.addEventListener("focusin", function(e) {
        if(!invoke(e, "target.matches", "input,textarea,select")) return;

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.add("focused");

        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.add("focused");
    });


    document.documentElement.addEventListener("blur", function(e) {
        if(!invoke(e, "target.matches", "input,textarea,select")) return;

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.remove("focused");

        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.remove("focused");
    });

    document.documentElement.addEventListener("focusout", function(e) {
        if(!invoke(e, "target.matches", "input,textarea,select")) return;

        var target = e.target;

        var label = target.closest("label");
        if(!!label) label.classList.remove("focused");

        var f = target.closest("fieldset");
        if(!!f) target.closest("fieldset").classList.remove("focused");
    });

    document.documentElement.addEventListener("reset", function(e) {
        if(!invoke(e, "target.matches", "form")) return;

        var target = e.target;

        each(target.querySelectorAll("input,textarea,select,button"), function(input) {
            input.dispatchEvent(new CustomEvent("change", { "bubbles": true }));
            window.setTimeout(function () { input.dispatchEvent(new CustomEvent("change", { "bubbles": true })); }, 10);
        });
    });

    document.documentElement.addEventListener("keydown", function(e) {
        if(e.ctrlKey || e.metaKey) switch (String.fromCharCode(e.which).toLowerCase()) {
            case 'p':
                e.preventDefault();

                (document.body || document.getElementsByTagName("body")[0]).dispatchEvent(new CustomEvent("print", { "bubbles": true }));

                document.documentElement.dispatchEvent(new CustomEvent("track", {
                    bubbles: true,
                    detail: {
                        category: "keyboard",
                        action: "print",
                        label: window.location.href,
                    },
                }));

                return false;
                break;
        }
    });
}