// Earths Radius. Needed because I'm a ball-earth theory guy.
const EarthsRadius = 6371e3;

// Get a (polar) vector from two cartesian coordinates.
// The results indicate distance (in meters) from point a to point b, and
// radians rotated from north, counterclockwise.
// The implementation uses haversine distance and initial bearing calculations
// as implemented here: https://www.movable-type.co.uk/scripts/latlong.html
// The function is optimized (by factoring out repeated calculations, renaming
// variables, and combining the bearing and distance code).
// The original code is located under the headings 'Distance' and 'Bearing'
export function vectorFromCoordinates(a, b) {
    const aLng = toRadians(a.lng);
    const bLng = toRadians(b.lng)
    const aLat = toRadians(a.lat);
    const bLat = toRadians(b.lat);

    const cosALat = Math.cos(aLat);
    const cosBLat = Math.cos(bLat);
    const sinDeltaLatSquared = Math.pow(Math.sin((bLat - aLat) / 2), 2);
    const sinDeltaLngSquared = Math.pow(Math.sin((bLng - aLng) / 2), 2);

    var a = sinDeltaLatSquared + cosALat * cosBLat * sinDeltaLngSquared;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const distance = EarthsRadius * c;

    var y = Math.sin(bLng - aLng) * cosBLat;
    var x = cosALat * Math.sin(bLat) - Math.sin(aLat) * cosBLat * Math.cos(bLng - aLng);

    const bearing = Math.atan2(y, x);

    return {
        bearing: bearing,
        distance: distance,
        units: 'meters',
    }
}

// This implements addition of lat/lng and a polar vector.
// The code is adapted from: https://www.movable-type.co.uk/scripts/latlong.html
// The function is optimized (by factoring out repeated calculations).
// The original code is located under heading 'Destination point given distance and bearing from start point'
export function addVectorToCoordinate(coor, vector) {
    const startLat = toRadians(coor.lat);
    const sinStartLat = Math.sin(startLat);
    const cosStartLat = Math.cos(startLat);

    const startLng = toRadians(coor.lng);

    const dist = vector.distance / EarthsRadius;
    const cosDist = Math.cos(dist);
    const sinDist = Math.sin(dist);

    const lat = Math.asin(sinStartLat * cosDist + cosStartLat * sinDist * Math.cos(vector.bearing));

    const deltaY = Math.sin(vector.bearing) * sinDist * cosStartLat;
    const deltaX = cosDist - sinStartLat * Math.sin(lat);
    const lng = startLng + Math.atan2(deltaY, deltaX);

    return {
        lat: toDegrees(lat),
        lng: ((toDegrees(lng) + 540) % 360) - 180,
    };
}
