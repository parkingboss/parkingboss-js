// Find angle at B for < ABC
export function findPixelAngle(a, b, c) {
    const AB = Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
    const BC = Math.sqrt(Math.pow(b.x - c.x, 2) + Math.pow(b.y - c.y, 2));
    const AC = Math.sqrt(Math.pow(c.x - a.x, 2) + Math.pow(c.y - a.y, 2));
    return Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB));
}

// Get a (polar) vector from two cartesian coordinates.
// The results indicate distance from point a to point b, and radians rotated
// counterclockwise starting at (1, 0).
export function vectorFromPoints(a, b) {
    const deltaX = b.x - a.x;
    const deltaY = b.y - a.y;
    return {
        bearing: Math.atan2(deltaY, deltaX),
        distance: Math.sqrt(deltaX * deltaX + deltaY * deltaY),
        units: 'pixels',
    };
}

// Add a polar vector to a point.
export function addVectorToPoint(point, vector) {
    return {
        x: point.x + (vector.distance * Math.cos(vector.bearing)),
        y: point.y + (vector.distance * Math.sin(vector.bearing)),
    }
}

// Convert degrees to radians
export function toRadians(x) {
    return x * Math.PI / 180;
}

// Convert radians to degrees
export function toDegrees(x) {
    return x * 180 / Math.PI;
}