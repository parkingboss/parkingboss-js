import { vectorFromPoints, addVectorToPoint } from './geometry';
import { vectorFromCoordinates, addVectorToCoordinate } from './geography';

export function buildMapper(a, b) {
    const abCartesian = vectorFromPoints(a, b);
    const abGeographical = vectorFromCoordinates(a, b);

    const geoToCartRatio = abGeographical.distance / abCartesian.distance;

    return {
        cartesianToGeographical: function (c) {
            const acCartesian = vectorFromPoints(a, c);
            const acGeographical = {
                distance: acCartesian.distance * geoToCartRatio,
                bearing: abGeographical.bearing - (abCartesian.bearing - acCartesian.bearing),
                units: 'meters',
            };

            return addVectorToCoordinate(a, acGeographical);
        },
        geographicalToCartesian: function (c) {
            const acGeographical = vectorFromCoordinates(a, c);
            const acCartesian = {
                distance: acGeographical.distance / geoToCartRatio,
                bearing: abCartesian.bearing - (abGeographical.bearing - acGeographical.bearing),
                units: 'pixels',
            };
            return addVectorToPoint(a, acCartesian);
        }
    };
}