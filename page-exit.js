import page from 'page';

export function pageExit() {
    page.exit("*", function(ctx, next) {
        document.documentElement.removeAttribute("data-records");
        document.documentElement.removeAttribute("data-record");
        document.documentElement.removeAttribute("data-record-view");
        next();
    });
}