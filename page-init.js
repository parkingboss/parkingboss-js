import page from 'page';
import qs from 'qs';

export function pageInit() {
    var prev;

    page("*", function(ctx, next) {

        if(!!qs) ctx.query = qs.parse(ctx.querystring);

        ctx.referer = prev;
        prev = ctx.path;

        next();

    });
}
