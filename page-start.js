import page from 'page';

export function pageStart() {
    page.start({
        dispatch:true,
        popstate:true,
        click:true,
        hashbang:false,
    });
}