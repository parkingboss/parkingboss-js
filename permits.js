import { get } from 'lodash-es';

function utc(obj) {
    return new Date(obj.utc || obj);
}

var numeric = /[^0-9]/g;
var timezone = /[-+]\d{2}:\d{2}$/g;

function local(datestr) {
    if(!datestr || !datestr.replace) return datestr;
    return datestr.replace(timezone, "");
}

export function validFrom(permit) {
    return get(permit, "valid.interval", "").split('/')[0];
}

export function validFromLocal(permit) {
    return local(validFrom(permit));
}

export function validTo(permit) {
    return get(permit, "valid.interval", "").split('/')[1];
}

export function validToLocal(permit) {
    return local(validTo(permit));
}

export function graceFrom(permit) {
    return get(permit, "grace.interval", "").split('/')[0];
}

export function graceTo(permit) {
    return get(permit, "grace.interval", "").split('/')[1];
}

export function issued(permit) {
    return get(permit, "issued.utc", "")
}

export function issuedLocal(permit) {
    return local(issued(permit));
}

export function expired(permit) {
    return get(permit, "valid.expired", permit.expired);
}

// null/undefined ends are considered indefinite
export function containsNow(aDate, bDate) {
    if(!aDate && !bDate) return true; // all time
    var now = Date.now();
    if(!aDate && !!bDate) return now <= utc(bDate).getTime();
    if(!!aDate && !bDate) return  utc(aDate).getTime() <= now;
    return utc(aDate).getTime() <= now && now <= utc(bDate).getTime();
}

export function isValidExcludingGrace(permit) {
    if(!!expired(permit)) return false; // fast check
    return containsNow(validFrom(permit), validTo(permit));
}

export function isValidIncludingGrace(permit) {
    return containsNow(graceFrom(permit) || validFrom(permit), graceTo(permit) || validTo(permit));
}

export function isValidOrGrace(permit) {
    return isValidIncludingGrace(permit);
}

// defaults to including grace
export function isValid(permit, grace) {
    if(grace === false) return isValidExcludingGrace(permit);
    return isValidIncludingGrace(permit);
}

export function isNotValid(permit, grace) {
    if(grace === false) return !isValidExcludingGrace(permit);
    return !isValidIncludingGrace(permit);
}

export function isInGracePeriod(permit) {
    return isValidIncludingGrace(permit) && !isValidExcludingGrace(permit); // is only in before/after grace period
}

// defaults to including grace
export function isPending(permit, grace) {
    if(!!expired(permit)) return false; // expired can't be pending in any way
    if(!validFrom(permit)) return false; // no start, can't be pending
    if(grace === false) return utc(validFrom(permit)).getTime() > Date.now();
    return utc(graceFrom(permit) || validFrom(permit)).getTime() > Date.now(); // is only in before/after grace period
}


// defaults to excluding grace
export function isExpired(permit, grace) {
    if(grace === false && !!expired(permit)) return true;
    if(!validTo(permit)) return false; // no end, can't be expired
    if(grace === false) return utc(validTo(permit)).getTime() < Date.now(); // only consider valid to
    return utc(graceTo(permit) || validTo(permit)).getTime() < Date.now(); // try grace first
}

export function isRevoked(permit) {
    return !!permit.assigned && isExpired(permit, false);
}

export function isValidOrPending(permit, grace) {
    return isValid(permit, grace) || isPending(permit, grace);
}

export function isValidOrPendingOrGrace(permit) {
    return isValidOrPending(permit, true);
}

export function isCancelled(permit) {
    if(!!get(permit, "cancelled") || !!get(permit, "valid.cancelled")) return true;
    if(!validFrom(permit) || !validTo(permit)) return false; // must be fully valid
    return utc(validFrom(permit)).getTime() == utc(validTo(permit)).getTime(); // start and end are the same
}

export function status(permit, grace, valid, cancelled, pending, revoked, expired) {
    if(isValid(permit, grace)) return !!valid || "" === valid ? valid : "valid";
    if(isCancelled(permit, grace)) return !!cancelled || "" === cancelled ? cancelled : "cancelled";
    if(isPending(permit, grace)) return !!pending || "" === pending ? pending : "pending";
    if(isRevoked(permit, grace)) return !!revoked || "" === revoked ? revoked : "revoked";
    if(isExpired(permit, grace)) return !!expired || "" === expired ? expired : "expired";
    return "";
}
