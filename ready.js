import { delay } from './delay';

export function ready() {
    // wait a minimum of 1s
    var $delay = delay(1000);
    document.documentElement.addEventListener("ready", function(e) {
        $delay.then(function() {
            document.documentElement.classList.add("ready");
        });
    });
}
