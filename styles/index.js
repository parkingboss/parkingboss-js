let bourbon = null;
try {
    bourbon = require('bourbon');
} catch (e) {
    console.warn("Bourbon was not installed. Parking Boss SCSS may not function properly");
}
const fs = require('fs');
const path = require('path');
const types = require('node-sass').types;

function svg(buffer) {
    const svg = buffer.toString()
        .replace(/\n/g, '')
        .replace(/\r/g, '')
        .replace(/\#/g, '%23')
        .replace(/\"/g, "'");

    return '"data:image/svg+xml;utf8,' + svg + '"';
}

function img(buffer, mime) {
    return 'url(data:' + mime + ';base64,' + buffer.toString('base64') + ')';
}

function sassInlineImage(options) {
    options = options || {};

    return {
        'inline-pb-img($file, $mime: null)': function(file, mime) {
            // we want to file relative to the base
            var filePath = path.resolve(__dirname, 'img', file.getValue());
            if (!fs.existsSync(filePath)) {
                console.error("\n\n\n" + 'File or inlining not found: ' + filePath + "\n\n\n")
            }

            var ext = filePath.split('.').pop();
            if (!mime) {
                mime = "image/" + ext;
            } else {
                mime = mime.getValue();
            }

            // read the file
            var data = fs.readFileSync(filePath);

            var buffer = new Buffer(data);
            var str = ext === 'svg' ? svg(buffer) : img(buffer, mime);
            return types.String(str);
        }
    };
};

module.exports = {
    sassConfig(opts) {
        opts = opts || {};
        return {
            sourceMap: opts.sourceMap != null ? opts.sourceMap : true,
            includePaths: [
                ...(bourbon && bourbon.includePaths || []),
                ...opts.includePaths || [],
            ],
            functions: {
                ...sassInlineImage({ base: opts.base }),
                ...(opts.functions || {}),
            },
        }
    }
};