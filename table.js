import { map, reduce } from 'lodash-es';

export function tableToCsv(table) {
    return map((table || root).querySelectorAll('thead tr, tbody tr'), (row) => {
        return map(row.querySelectorAll("th,td"), (col) => {
            return '"' + (reduce(col.querySelectorAll("ul.photos a"), (list, a) => {
                list.push(a.href);
                return list;
            }, []).join(", ") || col.textContent.replace(/"/g, '""')) + '"'; // escape double quotes
        }).join(",");
    }).join("\r\n");
}