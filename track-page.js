import page from 'page';
import { invoke } from 'lodash-es';

export function trackPages(ga, appInsights) {
    page("*", function (ctx, next) {
        invoke(ga, "push", ['_trackPageview', ctx.canonicalPath]);
        invoke(appInsights, "trackPageView", ctx.canonicalPath);

        next();
    });
}
