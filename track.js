export function track() {
    document.addEventListener("track", function(e) {
       var data = e.detail;
       if(!data) return;

        if (!!window._gaq) _gaq.push(['_trackEvent', data.category, data.action, data.label]);

        if(!!window.ga) ga('send', 'event', data.category, data.action, data.label);  // value is a number.
    });
}
